/*
 
console c program using  multiprecision libraries : 
- gmp
- mpfr
- mpc

based on the slides : 
https://www.gnu.org/ghm/2011/paris/slides/andreas-enge-mpc.pdf
 
compile : 
gcc s.c  -lmpc -lmpfr -lgmp

run :

./a.out


 
*/
 
 
#include <stdio.h>
#include <gmp.h>
#include <mpfr.h>
#include <mpc.h>



int DescribeInex(int inex)
{
printf ("inex = %d ; %i %i\n", inex, MPC_INEX_RE (inex), MPC_INEX_IM (inex));
if (inex==-1) printf(" bad input string \n\n") ;
else {if (MPC_INEX_RE (inex)==0) printf(" real part  value is exact as the input \n");
      if (MPC_INEX_RE (inex)<0) printf(" rounded real part  value is  less than the exact one input \n");
      if (MPC_INEX_RE (inex)>0) printf(" rounded real part  value is  grater than the exact one input \n");
      if (MPC_INEX_IM (inex)==0) printf(" imag part  value is exact as the input \n");
      if (MPC_INEX_IM (inex)<0) printf(" rounded imag part  value is  less than the exact one input \n");
      if (MPC_INEX_IM (inex)>0) printf(" rounded imag part  value is  grater than the exact one input \n");
      printf("  \n");
      }
 
return 0; 


}
 
 
int main (void) {
 
mpc_t z; // input value = op

int print_result;
int inex; // return value of the function ( not the result of computation )
//mpc_t r; // output value = rop : r=f(z)
 
// 
mpc_init2 (z, 123);
//mpc_init2 (r, 123); 


//   bad input  
inex = mpc_set_str (z, "(-1.8605aaa7396001587505e+00 -0.00000093437424500e+00)",10, MPC_RNDNN); // read z value from string 
// print result  
printf ("z = "); mpc_out_str (stdout, 10, 0, z, MPC_RNDNN); printf ("  \n");
printf ("z = "); mpfr_out_str (stdout, 10, 0, mpc_realref (z), MPFR_RNDD); printf (" ;  "); mpfr_out_str (stdout, 10, 0, mpc_imagref (z), MPFR_RNDD); printf (" \n "); 
DescribeInex( inex);


//   bad input  
inex = mpc_set_str (z, "(aa7396001587505e+00 -0.00000ggg093437424500e+00)",10, MPC_RNDNN); // read z value from string 
// print result  
printf ("z = "); mpc_out_str (stdout, 10, 0, z, MPC_RNDNN); printf ("  \n");
printf ("z = "); mpfr_out_str (stdout, 10, 0, mpc_realref (z), MPFR_RNDD); printf (" ;  "); mpfr_out_str (stdout, 10, 0, mpc_imagref (z), MPFR_RNDD); printf (" \n "); 
DescribeInex( inex);


//   good input  
inex = mpc_set_str (z, "(-1.86057396001587505e+00 -0.00000093437424500e+00)",10, MPC_RNDNN); // read z value from string 
// print result  
printf ("z = "); mpc_out_str (stdout, 10, 0, z, MPC_RNDNN); printf ("  \n");
printf ("z = "); mpfr_out_str (stdout, 10, 0, mpc_realref (z), MPFR_RNDD); printf (" ;  "); mpfr_out_str (stdout, 10, 0, mpc_imagref (z), MPFR_RNDD); printf (" \n "); 
DescribeInex( inex);


//   good input  
inex = mpc_set_str (z, "(-1 -0.00000093437424500e+00)",10, MPC_RNDNN); // read z value from string 
// print result  
printf ("z = "); mpc_out_str (stdout, 10, 0, z, MPC_RNDNN); printf ("  \n");
printf ("z = "); mpfr_out_str (stdout, 10, 0, mpc_realref (z), MPFR_RNDD); printf (" ;  "); mpfr_out_str (stdout, 10, 0, mpc_imagref (z), MPFR_RNDD); printf (" \n "); 
DescribeInex(inex);


//   good binary input  
inex = mpc_set_str (z, "0b101",0, MPC_RNDNN); // read z value from string 
// print result  
printf ("z = "); mpc_out_str (stdout, 2, 0, z, MPC_RNDNN); printf ("  \n");
printf ("z = "); mpfr_out_str (stdout, 2, 0, mpc_realref (z), MPFR_RNDD); printf (" ;  "); mpfr_out_str (stdout, 2, 0, mpc_imagref (z), MPFR_RNDD); printf (" \n "); 
DescribeInex(inex);


//   good binary input  ; If base = 0, then a prefix may be used to indicate the base in which the floating-point number is written.
inex = mpc_set_str (z, "(0b101 0b1.01)",0, MPC_RNDNN); // read z value from string 
// print result  
printf ("z = "); mpc_out_str (stdout, 2, 0, z, MPC_RNDNN); printf ("  \n");
printf ("z = "); mpfr_out_str (stdout, 2, 0, mpc_realref (z), MPFR_RNDD); printf (" ;  "); mpfr_out_str (stdout, 2, 0, mpc_imagref (z), MPFR_RNDD); printf (" \n "); 
DescribeInex(inex);

//   good binary input  ; If base = 0, then a prefix may be used to indicate the base in which the floating-point number is written.
inex = mpc_set_str (z, "(-0b1.0100e-2 +0b1.0100e+3)"  ,0, MPC_RNDNN); // read z value from string 
// print result  
printf ("z = "); mpc_out_str (stdout, 2, 0, z, MPC_RNDNN); printf("\n");
printf ("z = "); mpfr_out_str (stdout, 2, 0, mpc_realref (z), MPFR_RNDD); printf (" ;  "); mpfr_out_str (stdout, 2, 0, mpc_imagref (z), MPFR_RNDD); printf (" \n "); 
DescribeInex(inex);

 
mpc_clear(z);
//mpc_clear(r);
}
